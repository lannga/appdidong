package com.nhomapp.weather;

import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void getTime(){
        long second = 1601816400;
        Date currentDate = new Date(second*1000);
        //"dd:MM:yy:HH:mm:ss"
        DateFormat df = new SimpleDateFormat("HH.00");
        String a = df.format(currentDate);
    }
}