package com.nhomapp.weather.util.enums;

public enum Unit {
    imperial("˚F","miles/hour"),
    metric("˚C","meter/sec");
    private final String s, k;
    Unit(String s, String k) {
        this.s = s;
        this.k = k;
    }
    public String getDes(){
        return s+"-"+k;
    }

    public String getTemp(){
        return s;
    }
}
