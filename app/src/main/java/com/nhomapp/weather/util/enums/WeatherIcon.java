package com.nhomapp.weather.util.enums;

import com.nhomapp.weather.R;

public enum WeatherIcon {
    i01("4804-weather-sunny.json"),
    i02("4804-weather-sunny.json"),
    i03("4806-weather-windy.json"),
    i04("4806-weather-windy.json"),
    i09("4803-weather-storm.json"),
    i10("4803-weather-storm.json"),
    i11("4805-weather-thunder.json"),
    i13("4793-weather-snow.json"),
    i50("4806-weather-windy.json"),
    ;

    private final String iconId;
    WeatherIcon(String iconId) {
        this.iconId = iconId;
    }

    public String getIcon() {
        return iconId;
    }
}
