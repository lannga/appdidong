package com.nhomapp.weather.util;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.nhomapp.weather.R;
import com.nhomapp.weather.util.enums.Unit;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String API_KEY = "4a69238a3a89950f48b4b78e642a8fa9";
    public static final String LOCATION_KEY = "location";
    public static int theme = R.style.AppTheme;

    public static final String[] suggest={
            "Mang ô, áo mưa",
            "Mang"
    };


    public Constants constants;

    public static MutableLiveData<Unit> units= new MutableLiveData<>();

    public Constants getInstance(){
        if(constants == null){
            constants = new Constants();
        }
        return constants;
    }


    public static Map<String , Object> getInitParams(){
        Map<String, Object> initParams = new HashMap<>();
        initParams.put("appid", API_KEY);
        initParams.put("lang","vi");
        initParams.put("units",units.getValue());
        return initParams;
    }
}
