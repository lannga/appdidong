package com.nhomapp.weather.util;

import android.annotation.SuppressLint;
import android.graphics.Color;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.nhomapp.weather.R;
import com.nhomapp.weather.models.Daily;
import com.nhomapp.weather.models.FeelsLike;
import com.nhomapp.weather.models.MoreInformation;
import com.nhomapp.weather.models.Temperature;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChartUtil {
    private static final int DISTANCE = 20;
    private static final int MAX_VALUE = 7;

    private static void initChart(LineChart chart) {
        chart.setTouchEnabled(false);
        chart.setDrawGridBackground(false);
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisLeft().setAxisMinimum(0);
        chart.getAxisRight().setAxisMinimum(0);
        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setDrawAxisLine(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setTextSize(8f);
        chart.setDrawGridBackground(false);
        chart.getDescription().setEnabled(false);
        Legend l = chart.getLegend();
        l.setEnabled(false);
        chart.animateX(1000);
        chart.setExtraOffsets(20, 20, 20, 10);
    }

    private static void buildChart(List<Entry> data, LineChart chart, List<Integer> colors) {
        LineDataSet set1 = new LineDataSet(data, "");
        set1.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return (int) value + "˚";
            }
        });
        set1.setValueTextSize(10f);
        set1.setColor(chart.getContext().getResources().getColor(R.color.colorPrimary));
        set1.setCircleColors(colors);
        LineData lineData = new LineData(set1);
        chart.setData(lineData);
        initChart(chart);
    }

    public static void createDay(List<Daily> rawData, LineChart chart) {
        List<Entry> data = new ArrayList<>();
        List<Integer> colors = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        long day = now.getTimeInMillis() / 1000;
        for (int i = 0; i < MAX_VALUE; i++) {
            Daily info = rawData.get(i);
            data.add(new Entry(i * DISTANCE, (int) info.temp.day));
            if (info.getDt() == day) {
                colors.add(chart.getContext().getResources().getColor(R.color.colorPrimaryDark));
            } else {
                colors.add(chart.getContext().getResources().getColor(R.color.colorPrimary));
            }
        }
        chart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return Convert.getTime(rawData.get((int) value / DISTANCE).getDt(), "dd-MM");
            }
        });
        buildChart(data, chart, colors);
    }


    public static void createHour(List<MoreInformation> rawData, LineChart chart) {
        List<Entry> data = new ArrayList<>();
        List<Integer> colors = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        for (int i = 0; i < MAX_VALUE; i++) {
            MoreInformation info = rawData.get(i);
            data.add(new Entry(i * DISTANCE, (int) info.getTemp()));
            String h = Convert.getTime(info.getDt(), "HH");
            if (Integer.parseInt(h) == hour) {
                colors.add(chart.getContext().getResources().getColor(R.color.colorPrimaryDark));
            } else {
                colors.add(chart.getContext().getResources().getColor(R.color.colorPrimary));
            }
        }
        chart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return Convert.getTime(rawData.get((int) value / DISTANCE).getDt(), "HH:00");
            }
        });
        buildChart(data, chart, colors);
    }

    public static void createWeatherTemp(Temperature temperature, FeelsLike feelsLike, LineChart chart) {
        List<Entry> data = new ArrayList<>();
        List<Entry> data1 = new ArrayList<>();
        int y = 0, y1 = 0;
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    y = (int) temperature.morn;
                    y1 = (int) feelsLike.morn+20;
                    break;
                case 1:
                    y = (int) temperature.eve;
                    y1 = (int) feelsLike.eve +20;
                    break;
                case 2:
                    y = (int) temperature.night;
                    y1 = (int) feelsLike.night+20;
                    break;
            }
            data.add(new Entry(i , y));
            data1.add(new Entry(i , y1));
        }
        List<LineDataSet> lineDataSets = new ArrayList<>();
        LineDataSet set = new LineDataSet(data, "");
        LineDataSet set1 = new LineDataSet(data1, "");
        set.setValueTextSize(10f);
        set1.setValueTextSize(10f);
        set1.setValueTextColor(Convert.getColor(chart.getContext(), android.R.attr.textColor));
        set.setValueTextColor(Convert.getColor(chart.getContext(), android.R.attr.textColor));
        set.setColor(chart.getContext().getResources().getColor(R.color.colorPrimary));
        set1.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf(value -20);
            }
        });
        List<ILineDataSet> dataSets = new ArrayList<>(lineDataSets);
        dataSets.add(set);
        dataSets.add(set1);
        LineData lineData = new LineData(dataSets);
        chart.setData(lineData);
        chart.getXAxis().setLabelCount(3, true);
        chart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                switch ((int) value) {
                    case 0:
                        return "Sáng";
                    case 1:
                        return "Chiều";
                    case 2:
                        return "Tối";
                    default:
                        return "";
                }
            }
        });
//        chart.getXAxis().setEnabled(false);
        initChart(chart);
    }


}
