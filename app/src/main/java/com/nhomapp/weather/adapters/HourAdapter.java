package com.nhomapp.weather.adapters;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.nhomapp.weather.models.Information;
import com.nhomapp.weather.models.MoreInformation;
import com.nhomapp.weather.util.Convert;

import java.util.Arrays;
import java.util.List;

public class HourAdapter extends WeatherAdapter<MoreInformation>{

    public HourAdapter(List<MoreInformation> list) {
        super(list);
        checkList = new Boolean[48];
        Arrays.fill(checkList, Boolean.FALSE);
    }

    @Override
    void createChart(LineChart chart, TextView temp, TextView feelLike, TextView time, RelativeLayout weatherChart, MoreInformation info) {
        chart.setVisibility(View.GONE);
        temp.setText(String.valueOf((int)info.getTemp()));
        feelLike.setText(String.valueOf((int)info.getFeels_like()));
        weatherChart.setVisibility(View.GONE);
        time.setText(Convert.getTime(info.getDt(),"HH:mm"));
    }
}
