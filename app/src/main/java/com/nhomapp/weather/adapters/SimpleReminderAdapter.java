package com.nhomapp.weather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nhomapp.weather.R;
import com.nhomapp.weather.models.reminder.Reminder;

import java.util.List;

public class SimpleReminderAdapter extends RecyclerView.Adapter<SimpleReminderAdapter.ViewHolder> {
    private List<Reminder> list;

    public SimpleReminderAdapter(List<Reminder> list){
        this.list = list;
    }

    public void setList(List<Reminder> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_simple_reminder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Reminder reminder = list.get(position);
        holder.content.setText(reminder.getContent());
        holder.check.setChecked(reminder.isDone());

        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView content;
        RadioButton check;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.content_tv);
            check = itemView.findViewById(R.id.done_rb);
        }
    }
}
