package com.nhomapp.weather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.github.mikephil.charting.charts.LineChart;
import com.nhomapp.weather.R;
import com.nhomapp.weather.models.Daily;
import com.nhomapp.weather.models.Information;
import com.nhomapp.weather.models.MoreInformation;
import com.nhomapp.weather.models.reminder.Reminder;
import com.nhomapp.weather.util.ChartUtil;
import com.nhomapp.weather.util.enums.WeatherIcon;

import java.util.Arrays;
import java.util.List;

abstract class WeatherAdapter<T extends Information> extends RecyclerView.Adapter<WeatherAdapter<T>.ViewHolder> {
    private List<T> list;
    private int currentPosition=-1;
    Boolean[] checkList;

    public WeatherAdapter(List<T> list){
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        T info =  list.get(position);
        holder.humidity.setText(String.valueOf(info.getHumidity()));
        holder.description.setText(info.getWeather().get(0).description);
        holder.uvIndex.setText(String.valueOf(info.getUvi()));
        holder.pop.setText(String.valueOf(info.getPop()));
        holder.windSpeed.setText(String.valueOf(info.getWind_speed()));
        WeatherIcon icon = WeatherIcon.valueOf(info.getWeather().get(0).getIcon());
        holder.wIcon.setAnimation(icon.getIcon());
//        holder.icon.setImageResource(icon.getIcon());
        createChart(holder.lineChart, holder.temp, holder.feelLike,holder.time,holder.weatherChart, info);
        if(this.currentPosition == position){
            if(!checkList[position])
                holder.weatherDetail.setVisibility(View.VISIBLE);
            else
                holder.weatherDetail.setVisibility(View.GONE);
            checkList[position] = !checkList[position];
        }
        holder.weatherItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = position;
                notifyItemChanged(position);
            }
        });
    }



    abstract void createChart(LineChart chart, TextView temp, TextView feelLike,TextView time,RelativeLayout weatherChart, T info);

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView temp, humidity, description, time, feelLike, uvIndex, pop, windSpeed;
        ImageView icon;
        LottieAnimationView wIcon;
        LineChart lineChart;
        LinearLayout weatherDetail, weatherItem;
        RelativeLayout weatherChart;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            temp = itemView.findViewById(R.id.temp);
            humidity = itemView.findViewById(R.id.humidity);
            description = itemView.findViewById(R.id.text_view_description);
            time = itemView.findViewById(R.id.time);
            feelLike = itemView.findViewById(R.id.feel_like);
            uvIndex = itemView.findViewById(R.id.uv_index);
            pop = itemView.findViewById(R.id.pop);
            windSpeed = itemView.findViewById(R.id.wind_speed);
            icon = itemView.findViewById(R.id.icon);
            lineChart = itemView.findViewById(R.id.chart);
            weatherDetail = itemView.findViewById(R.id.weather_detail);
            weatherItem = itemView.findViewById(R.id.weather_item);
            weatherChart = itemView.findViewById(R.id.weather_chart);
            wIcon = itemView.findViewById(R.id.w_icon);
        }


    }
}

