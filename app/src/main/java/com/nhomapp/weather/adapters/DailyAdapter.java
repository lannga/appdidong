package com.nhomapp.weather.adapters;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.nhomapp.weather.models.Daily;
import com.nhomapp.weather.util.ChartUtil;
import com.nhomapp.weather.util.Convert;

import java.util.Arrays;
import java.util.List;

public class DailyAdapter extends WeatherAdapter<Daily>{

    public DailyAdapter(List<Daily> list) {
        super(list);
        checkList = new Boolean[8];
        Arrays.fill(checkList, Boolean.FALSE);
    }

    @Override
    void createChart(LineChart chart, TextView temp, TextView feelLike, TextView time, RelativeLayout weatherChart, Daily info) {
        ChartUtil.createWeatherTemp(info.temp, info.feels_like, chart);
        temp.setText(String.valueOf((int)info.temp.day));
        feelLike.setText(String.valueOf((int)info.feels_like.day));
        time.setText(Convert.getTime(info.getDt(),"dd-MM"));
    }
}
