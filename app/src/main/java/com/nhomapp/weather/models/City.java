package com.nhomapp.weather.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "city")
public class City {
    @PrimaryKey
    public int id;
    public String name;
    public String state;
    public String country;
    public String country_full_name;
    public double lat;
    public double lon;

    @Ignore
    public String getAddress() {
        return name + ", " + country_full_name;
    }


}
