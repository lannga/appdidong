package com.nhomapp.weather.models;

public class Weather {
    public int id;
    public String description;
    public String icon;

    public String getIcon() {
        return "i"+icon.replaceAll("\\D+", "");
    }
}
