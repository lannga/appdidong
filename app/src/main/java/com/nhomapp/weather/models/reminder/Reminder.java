package com.nhomapp.weather.models.reminder;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "reminder")
public class Reminder {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String dTime;
    private String content;
    private boolean isDone = false;

    public Reminder() {
    }
    @Ignore
    public Reminder(String dTime, String content, boolean isDone) {
        this.dTime = dTime;
        this.content = content;
        this.isDone = isDone;
    }
    @Ignore
    public Reminder(String dTime, String content) {
        this.dTime = dTime;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }


    public String getDTime() {
        return dTime;
    }

    public void setDTime(String dTime) {
        this.dTime = dTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
