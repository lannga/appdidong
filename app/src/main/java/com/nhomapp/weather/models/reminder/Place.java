package com.nhomapp.weather.models.reminder;



public class Place {
    public String city;
    public String country;
    public double lat;
    public double lng;

    public String getAddress() {
        return city + ", " + country;
    }


}
