package com.nhomapp.weather.models;

import java.io.Serializable;
import java.util.List;

public class DataResponse implements Serializable {
    public int cod;
    public String message;
    public double lat;
    public double lon;
    public MoreInformation current;
    public List<MoreInformation> hourly;
    public List<Daily> daily;
}
