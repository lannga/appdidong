package com.nhomapp.weather.persistence;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.nhomapp.weather.models.City;
import com.nhomapp.weather.models.reminder.Reminder;

@Database(entities = {City.class, Reminder.class},
        version = 1,
        exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "app_db";

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME)
                    .createFromAsset("app_db.db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract CityAccess cityAccess();
    public abstract ReminderAccess reminderAccess();


}



