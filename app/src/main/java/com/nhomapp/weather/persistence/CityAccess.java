package com.nhomapp.weather.persistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.nhomapp.weather.models.City;
import com.nhomapp.weather.models.reminder.Reminder;

import java.util.List;

@Dao
public interface CityAccess {
    @Query("SELECT * FROM city WHERE  (name || country_full_name) LIKE  '%' || :search || '%'")
    List<City> searchCity(String search);

    @Query("SELECT * FROM city")
    List<City> getAll();

}


