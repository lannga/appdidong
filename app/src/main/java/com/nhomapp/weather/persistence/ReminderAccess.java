package com.nhomapp.weather.persistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.nhomapp.weather.models.reminder.Reminder;

import java.util.List;

@Dao
public interface ReminderAccess {

    @Insert
    void addReminder(Reminder reminder);

    @Query("SELECT * FROM reminder")
    List<Reminder> getAll();

    @Query("SELECT * FROM reminder WHERE dTime= :time")
    public List<Reminder> getByTime(String time);
}


