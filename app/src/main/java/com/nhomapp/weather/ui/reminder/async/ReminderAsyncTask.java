//package com.nhomapp.weather.ui.reminder.async;
//
//import android.app.Application;
//import android.os.AsyncTask;
//
//import androidx.lifecycle.MutableLiveData;
//
//import com.nhomapp.weather.models.reminder.Reminder;
//import com.nhomapp.weather.persistence.AppDatabase;
//import com.nhomapp.weather.persistence.ReminderAccess;
//
//import java.util.Calendar;
//import java.util.List;
//
//public class ReminderAsyncTask extends AsyncTask<Void, Void, List<Reminder>> {
//
//    public static String DEL="DEL";
//    public static String GET_ALL="GET";
//
//    private void provideList(Reminder reminder){
//        final Calendar c = Calendar.getInstance();
//        int mYear, mMonth, mDay, mHour, mMinute;
//        mYear = c.get(Calendar.YEAR);
//        mMonth = c.get(Calendar.MONTH) + 1;
//        mDay = c.get(Calendar.DAY_OF_MONTH);
//        if(reminder.getDTime().equals(mDay+"-"+mMonth+"-"+mYear)){
//            if(todayList != null)
//                todayList.getValue().add(reminder);
//        }else{
//            if(otherList != null)
//                otherList.getValue().add(reminder);
//        }
//    }
//
//    private MutableLiveData<List<Reminder>> todayList;
//    private MutableLiveData<List<Reminder>> otherList;
//    private Reminder reminder;
//    private ReminderAccess reminderAccess;
//    private String type = "CREATE";
//
//    ReminderAsyncTask(Application application){
//        reminderAccess = AppDatabase.getInstance(application).reminderAccess();
//    }
//    ReminderAsyncTask(Application application, Reminder reminder, MutableLiveData<List<Reminder>> todayList, MutableLiveData<List<Reminder>> otherList){
//        this(application);
//        this.reminder = reminder;
//        this.todayList = todayList;
//        this.otherList = otherList;
//    }
//
//    ReminderAsyncTask(Application application,MutableLiveData<List<Reminder>> todayList,MutableLiveData<List<Reminder>> otherList){
//        this(application);
//        type ="GET";
//        this.todayList = todayList;
//        this.otherList = otherList;
//    }
//
//    ReminderAsyncTask(Application application,MutableLiveData<List<Reminder>> todayList){
//        this(application);
//        type ="GET";
//        this.todayList = todayList;
//    }
//
//    @Override
//    protected List<Reminder> doInBackground(Void... voids) {
//        switch (type){
//            case "CREATE":
//                reminderAccess.addReminder(reminder);
//                return null;
//            case "DEL":
//                break;
//            case "GET":
//                List<Reminder> reminders = reminderAccess.getAll();
//                return reminders;
//        }
//    }
//
//    @Override
//    protected void onPostExecute(List<Reminder> reminders) {
//        super.onPostExecute(reminders);
//        if(reminder != null){
//            provideList(reminder);
//        }else{
//            for (Reminder r: reminders) {
//                provideList(r);
//            }
//        }
//    }
//}
