package com.nhomapp.weather.ui.main;

import android.app.Application;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nhomapp.weather.models.DataResponse;
import com.nhomapp.weather.models.reminder.Reminder;
import com.nhomapp.weather.persistence.AppDatabase;
import com.nhomapp.weather.persistence.ReminderAccess;
import com.nhomapp.weather.requests.ApiService;
import com.nhomapp.weather.util.Constants;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends AndroidViewModel implements Callback<DataResponse> {
    private MutableLiveData<DataResponse> mDataResponse;
    private MutableLiveData<Location> mLocation;
    private MutableLiveData<List<Reminder>> todayList;

    public MainViewModel(@NonNull Application application) {
        super(application);
        mDataResponse = new MutableLiveData<>();
        mLocation = new MutableLiveData<>();
        todayList = new MutableLiveData<>();
        new ReminderAction().execute();
    }

    public void setMLocation(Location location) {
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        Map<String, Object> params = Constants.getInitParams();
        params.put("lat", lat);
        params.put("lon", lon);
        Call<DataResponse> call = ApiService.getApi().getAll(params);
        call.enqueue(this);
        if(!location.getProvider().equals("")){
            mLocation.setValue(location);
            return;
        }
        Geocoder geocoder = new Geocoder(getApplication().getApplicationContext(), Locale.getDefault());
        List<Address> listAddresses = null;
        try {
            listAddresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (null != listAddresses && listAddresses.size() > 0) {
            String loc = listAddresses.get(0).getAddressLine(0);
            location.setProvider(loc);
            mLocation.setValue(location);
        }
    }

    public LiveData<Location> getMLocation() {
        return mLocation;
    }

    public LiveData<DataResponse> getDataResponse() {
        return mDataResponse;
    }
    public LiveData<List<Reminder>> getTodayList() {
        return todayList;
    }


    @Override
    public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
        DataResponse dataResponse = response.body();
        if (dataResponse.cod != 0) {
            return;
        }
        mDataResponse.setValue(dataResponse);
    }

    @Override
    public void onFailure(Call<DataResponse> call, Throwable t) {
    }

    class ReminderAction extends AsyncTask<Void, Void, List<Reminder>> {
        @Override
        protected List<Reminder> doInBackground(Void... voids) {
            final Calendar c = Calendar.getInstance();
            int mYear, mMonth, mDay, mHour, mMinute;
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH) + 1;
            mDay = c.get(Calendar.DAY_OF_MONTH);
            ReminderAccess reminderAccess = AppDatabase.getInstance(getApplication()).reminderAccess();
            return reminderAccess.getByTime(mDay+"-"+mMonth+"-"+mYear);
        }

        @Override
        protected void onPostExecute(List<Reminder> reminders) {
            super.onPostExecute(reminders);
            todayList.setValue(reminders);
        }
    }
}
