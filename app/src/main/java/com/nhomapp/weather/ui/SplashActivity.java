package com.nhomapp.weather.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.nhomapp.weather.R;
import com.nhomapp.weather.ui.main.MainActivity;
import com.nhomapp.weather.util.Constants;
import com.nhomapp.weather.util.enums.Unit;


public class SplashActivity extends AppCompatActivity implements LocationListener {
    private LocationManager locationManager;
    private Location location;
    private boolean isReadingCityFile = false;

    private void getSetting(){
        SharedPreferences sp = getSharedPreferences("app", MODE_PRIVATE);
        String unit = sp.getString("units", Unit.metric.toString());
        Constants.units.setValue(Unit.valueOf(unit));
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 0
            );
            return;
        }
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 1000, 1000, this);
    }

    private void goToMainActivity() {
        if(isReadingCityFile){
            return;
        }
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        if(location != null){
            intent.putExtra(Constants.LOCATION_KEY, location);
        }
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LottieAnimationView icon = findViewById(R.id.icon);
        getSetting();
        if(isNetworkConnected())
            getLocation();
        else{
            LinearLayout noInternet = findViewById(R.id.no_internet);
            icon.setVisibility(View.GONE);
            noInternet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        goToMainActivity();
    }

    @Override
    public void onLocationChanged(Location loc) {
        location = loc;
        locationManager.removeUpdates(this);
        goToMainActivity();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

}