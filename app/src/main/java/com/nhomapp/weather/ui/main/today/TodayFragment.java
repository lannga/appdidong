package com.nhomapp.weather.ui.main.today;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.nhomapp.weather.adapters.SimpleReminderAdapter;
import com.nhomapp.weather.models.Weather;
import com.nhomapp.weather.models.reminder.Reminder;
import com.nhomapp.weather.ui.main.MainActivity;
import com.nhomapp.weather.ui.main.MainViewModel;
import com.nhomapp.weather.R;
import com.nhomapp.weather.models.DataResponse;
import com.nhomapp.weather.ui.reminder.ReminderListActivity;
import com.nhomapp.weather.util.Constants;
import com.nhomapp.weather.util.enums.Unit;
import com.nhomapp.weather.util.enums.WeatherIcon;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TodayFragment extends Fragment {

    private TodayViewModel todayViewModel;
    private MainViewModel mainViewModel;

    public void init(View root){
        TextView feelLikeLabel =(TextView) root.findViewById(R.id.feel_like).findViewById(R.id.label);
        feelLikeLabel.setText("Cảm giác");
        TextView humidityLabel =(TextView) root.findViewById(R.id.humidity).findViewById(R.id.label);
        humidityLabel.setText("Độ ẩm");
        TextView windSpeedLabel =(TextView) root.findViewById(R.id.wind_speed).findViewById(R.id.label);
        windSpeedLabel.setText("Tốc độ gió");
        TextView uvIndexLabel =(TextView) root.findViewById(R.id.uv_index).findViewById(R.id.label);
        uvIndexLabel.setText("Chỉ số UV");
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        todayViewModel =
                ViewModelProviders.of(this).get(TodayViewModel.class);

        MainActivity mainActivity = (MainActivity) getActivity();
        mainViewModel =  mainActivity.getMainViewModel();
        View root = inflater.inflate(R.layout.fragment_today, container, false);
        init(root);
        TextView feelLike =(TextView) root.findViewById(R.id.feel_like).findViewById(R.id.value);
        TextView humidity =(TextView) root.findViewById(R.id.humidity).findViewById(R.id.value);
        TextView windSpeed =(TextView) root.findViewById(R.id.wind_speed).findViewById(R.id.value);
        TextView uvIndex =(TextView) root.findViewById(R.id.uv_index).findViewById(R.id.value);

        TextView temp = (TextView) root.findViewById(R.id.text_view_temp);
        TextView description = (TextView) root.findViewById(R.id.text_view_description);

        ImageView imageView = root.findViewById(R.id.weather_icon);
        LottieAnimationView animationView = root.findViewById(R.id.w_icon);

        TextView allRemind = root.findViewById(R.id.all_remind);
        allRemind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ReminderListActivity.class));
            }
        });

        RecyclerView listView = root.findViewById(R.id.list_view_reminder);
        List<Reminder> list = new ArrayList<>();
        list.add(new Reminder());
        list.add(new Reminder());
        list.add(new Reminder());
        SimpleReminderAdapter simpleReminderAdapter = new SimpleReminderAdapter(list);
        listView.setLayoutManager(new LinearLayoutManager(container.getContext(),  LinearLayoutManager.HORIZONTAL, false));
        listView.setAdapter(simpleReminderAdapter);

        mainViewModel.getDataResponse().observe(getViewLifecycleOwner(), new Observer<DataResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChanged(DataResponse dataResponse) {
                feelLike.setText(dataResponse.current.getFeels_like()+Constants.units.getValue().getTemp());
                humidity.setText(dataResponse.current.getHumidity()+"%");
                windSpeed.setText(dataResponse.current.getWind_speed()+"");
                uvIndex.setText(dataResponse.current.getUvi()+"");

                temp.setText((int)dataResponse.current.getTemp()+"");
//                description.setText(dataResponse.current.getWeather().get(0).description);

                WeatherIcon icon = WeatherIcon.valueOf(dataResponse.current.getWeather().get(0).getIcon());
                animationView.setAnimation(icon.getIcon());
                animationView.playAnimation();
//                imageView.setImageResource(icon.getIcon());

            }
        });

        mainViewModel.getTodayList().observe(getViewLifecycleOwner(), new Observer<List<Reminder>>() {
            @Override
            public void onChanged(List<Reminder> reminders) {
                simpleReminderAdapter.setList(reminders);
            }
        });

        TextView unitView = root.findViewById(R.id.temp_unit);
        unitView.setText(Constants.units.getValue().getTemp());
        Constants.units.observe(getViewLifecycleOwner(), new Observer<Unit>() {
            @Override
            public void onChanged(Unit unit) {
                unitView.setText(unit.getTemp());
                if(mainViewModel.getMLocation().getValue() != null)
                    mainViewModel.setMLocation(mainViewModel.getMLocation().getValue());
            }
        });
        return root;
    }

}
