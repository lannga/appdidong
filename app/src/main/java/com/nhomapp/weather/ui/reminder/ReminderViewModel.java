package com.nhomapp.weather.ui.reminder;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nhomapp.weather.models.City;
import com.nhomapp.weather.models.reminder.Reminder;
import com.nhomapp.weather.persistence.AppDatabase;
import com.nhomapp.weather.persistence.CityAccess;
import com.nhomapp.weather.persistence.ReminderAccess;
import com.nhomapp.weather.ui.search.SearchViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReminderViewModel extends AndroidViewModel {

    private MutableLiveData<List<Reminder>> todayList;
    private MutableLiveData<List<Reminder>> otherList;
    private ReminderAccess reminderAccess;

    public ReminderViewModel(Application application) {
        super(application);
        reminderAccess = AppDatabase.getInstance(getApplication()).reminderAccess();
        todayList = new MutableLiveData<>();
        todayList.setValue(new ArrayList<>());
        otherList = new MutableLiveData<>();
        otherList.setValue(new ArrayList<>());
        getAllRemind();

    }

    public LiveData<List<Reminder>> getTodayList(){
        return todayList;
    }

    public LiveData<List<Reminder>> getOtherList(){
        return otherList;
    }

    public void addNewOne(Reminder reminder){
        new ReminderAction(reminder).execute();
    }

    public void getAllRemind(){
        new ReminderAction(null).execute();
    }


    class ReminderAction extends AsyncTask<Void, Void, List<Reminder>> {



        private void provideList(Reminder reminder){
            final Calendar c = Calendar.getInstance();
            int mYear, mMonth, mDay, mHour, mMinute;
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH) + 1;
            mDay = c.get(Calendar.DAY_OF_MONTH);
            if(reminder.getDTime().equals(mDay+"-"+mMonth+"-"+mYear)){
                todayList.getValue().add(reminder);
            }else{
                otherList.getValue().add(reminder);
            }
        }


        private Reminder reminder;
        ReminderAction(Reminder reminder){
            this.reminder = reminder;
        }

        @Override
        protected List<Reminder> doInBackground(Void... voids) {
            if(reminder!= null){
                reminderAccess.addReminder(reminder);
                return null;
            }else{
                List<Reminder> reminders = reminderAccess.getAll();
                return reminders;
            }
        }

        @Override
        protected void onPostExecute(List<Reminder> reminders) {
            super.onPostExecute(reminders);
            if(reminder != null){
                provideList(reminder);
            }else{
                for (Reminder r: reminders) {
                    provideList(r);
                }
            }
            todayList.setValue(todayList.getValue());
            otherList.setValue(otherList.getValue());
        }
    }
}