package com.nhomapp.weather.ui.reminder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Service;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nhomapp.weather.R;
import com.nhomapp.weather.adapters.ReminderAdapter;
import com.nhomapp.weather.adapters.ReminderSuggestionAdapter;
import com.nhomapp.weather.models.reminder.Reminder;
import com.nhomapp.weather.persistence.AppDatabase;
import com.nhomapp.weather.ui.main.MainViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReminderListActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private BottomSheetBehavior bottomSheetBehavior;
    private InputMethodManager keyboard;
    private EditText contentInput;
    private TextView timeView;
    private LinearLayout timeChose;
    private ImageView confirmView;
    private ReminderViewModel reminderViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_reminder_list);
        super.onCreate(savedInstanceState);
        reminderViewModel= ViewModelProviders.of(this).get(ReminderViewModel.class);
        RecyclerView today = findViewById(R.id.reminder_today);
        today.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView upcoming = findViewById(R.id.reminder_upcoming);
        upcoming.setLayoutManager(new LinearLayoutManager(this));

        ReminderAdapter todayAdapter = new ReminderAdapter(reminderViewModel.getTodayList().getValue());
        ReminderAdapter upcomingAdapter = new ReminderAdapter(reminderViewModel.getOtherList().getValue());

        today.setAdapter(todayAdapter);
        upcoming.setAdapter(upcomingAdapter);

        contentInput = findViewById(R.id.content_edit_text);
        timeView = findViewById(R.id.time_view);
        timeChose = findViewById(R.id.time_chose);
        timeChose.setOnClickListener(this);
        confirmView = findViewById(R.id.confirm);
        confirmView.setOnClickListener(this);

        LinearLayout reminderBottom = findViewById(R.id.reminder_create);
        LinearLayout outsideBottom = findViewById(R.id.bottom_outside);
        outsideBottom.setOnClickListener(this);
        bottomSheetBehavior = BottomSheetBehavior.from(reminderBottom);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        FloatingActionButton createButton = findViewById(R.id.create_button);
        createButton.setOnClickListener(this);

        keyboard = (InputMethodManager)this.getSystemService(Service.INPUT_METHOD_SERVICE);
        reminderViewModel.getTodayList().observe(this, list ->{
            if(list.isEmpty()){
                today.setVisibility(View.GONE);
            }
            todayAdapter.notifyDataSetChanged();
        });

        reminderViewModel.getOtherList().observe(this, list ->{
            if(list.isEmpty()){
                upcoming.setVisibility(View.GONE);
            }
            upcomingAdapter.notifyDataSetChanged();
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.create_button:
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return;
            case R.id.bottom_outside:
                if(keyboard.isAcceptingText())
                    keyboard.hideSoftInputFromWindow(contentInput.getWindowToken(),0);
                else
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                return;
            case R.id.time_chose:
                int mYear, mMonth, mDay, mHour, mMinute;
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(this,this, mYear, mMonth, mDay);
                datePickerDialog.show();
                return;
            case R.id.confirm:
                reminderViewModel.addNewOne(new Reminder(timeView.getText().toString(),contentInput.getText().toString()));
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                return;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        timeView.setText(dayOfMonth+"-"+(month+1)+"-"+year);
//        TimePickerDialog timePickerDialog = new TimePickerDialog(this,this, 0, 0, false);
//        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        timeView.setText("");
    }
}