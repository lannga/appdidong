package com.nhomapp.weather.ui.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nhomapp.weather.R;
import com.nhomapp.weather.ui.BaseActivity;
import com.nhomapp.weather.ui.reminder.ReminderListActivity;
import com.nhomapp.weather.util.Constants;
import com.nhomapp.weather.util.enums.Unit;

public class MenuActivity extends BaseActivity implements View.OnClickListener {
    private TextView unitVal;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_menu);
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences("app", MODE_PRIVATE);
        editor = sp.edit();

        LinearLayout lang = findViewById(R.id.lang);
        lang.setOnClickListener(this);
        LinearLayout theme = findViewById(R.id.theme);
        theme.setOnClickListener(this);
        LinearLayout unit = findViewById(R.id.unit);
        unit.setOnClickListener(this);
        unitVal = findViewById(R.id.unit_val);
        unitVal.setText(Constants.units.getValue().getDes());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lang:

                return;
            case R.id.theme:
                if(sp.getInt("theme",R.style.AppTheme) == R.style.AppTheme)
                    editor.putInt("theme", R.style.DarkTheme);
                else
                    editor.putInt("theme", R.style.AppTheme);
                editor.apply();
                break;
            case R.id.unit:
                if(Constants.units.getValue() == Unit.metric){
                    Constants.units.setValue(Unit.imperial);
                }else{
                    Constants.units.setValue(Unit.metric);
                }
                unitVal.setText(Constants.units.getValue().getDes());
                editor.putString("units", Constants.units.getValue().toString());
                editor.apply();
                break;
        }
    }
}