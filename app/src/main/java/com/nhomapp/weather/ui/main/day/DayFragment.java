package com.nhomapp.weather.ui.main.day;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.LineChart;
import com.nhomapp.weather.R;
import com.nhomapp.weather.adapters.DailyAdapter;
import com.nhomapp.weather.models.Daily;
import com.nhomapp.weather.models.DataResponse;
import com.nhomapp.weather.ui.main.MainActivity;
import com.nhomapp.weather.ui.main.MainViewModel;
import com.nhomapp.weather.util.ChartUtil;

import java.util.ArrayList;
import java.util.List;

public class DayFragment extends Fragment {

    private MainViewModel mainViewModel;
    private List<Daily> dailyList;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_day, container, false);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainViewModel =  mainActivity.getMainViewModel();
        LineChart chart = root.findViewById(R.id.chart);
        dailyList = new ArrayList<>();
        DailyAdapter dailyAdapter = new DailyAdapter(dailyList);
        RecyclerView daily = root.findViewById(R.id.daily_list);
        daily.setLayoutManager(new LinearLayoutManager(container.getContext()));
        daily.setAdapter(dailyAdapter);
        mainViewModel.getDataResponse().observe(getViewLifecycleOwner(), new Observer<DataResponse>() {
            @Override
            public void onChanged(DataResponse dataResponse) {
                ChartUtil.createDay(dataResponse.daily, chart);
                dailyList.clear();
                dailyList.addAll(dataResponse.daily);
                dailyAdapter.notifyDataSetChanged();
            }
        });
        return root;
    }
}
