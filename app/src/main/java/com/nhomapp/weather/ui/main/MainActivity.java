package com.nhomapp.weather.ui.main;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nhomapp.weather.R;
import com.nhomapp.weather.ui.BaseActivity;
import com.nhomapp.weather.ui.menu.MenuActivity;
import com.nhomapp.weather.ui.search.SearchActivity;
import com.nhomapp.weather.util.Constants;
import com.nhomapp.weather.util.Convert;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.Calendar;

public class MainActivity extends BaseActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, NavController.OnDestinationChangedListener {

    private MainViewModel mainViewModel;
    private TextView textLocation;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BottomNavigationView navView;
    private LinearLayout locationChose;

    public MainViewModel getMainViewModel() {
        return mainViewModel;
    }

    public void init(){
            try {
                Location location = (Location) getIntent().getExtras().getParcelable(Constants.LOCATION_KEY);
                location.setProvider("");
                mainViewModel.setMLocation(location);
            }catch (NullPointerException ex){
                goToSearchActivity();
            }

    }


    public void goToSearchActivity() {
        startActivityForResult(new Intent(MainActivity.this, SearchActivity.class), 2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        textLocation = findViewById(R.id.tev_location);
        locationChose = findViewById(R.id.lo_chose);
        locationChose.setOnClickListener(this);

        ImageView imageView = findViewById(R.id.menu);
        imageView.setOnClickListener(this);

        swipeRefreshLayout = findViewById(R.id.main_activity);
        swipeRefreshLayout.setOnRefreshListener(this);

        navView = findViewById(R.id.nav_view);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.addOnDestinationChangedListener(this);
        NavigationUI.setupWithNavController(navView, navController);
        mainViewModel.getMLocation().observe(this, location -> {
            textLocation.setText(location.getProvider());
        });
        mainViewModel.getDataResponse().observe(this, dataResponse -> {
            swipeRefreshLayout.setRefreshing(false);
        });
        init();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lo_chose:
                goToSearchActivity();
                return;
            case R.id.menu:
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
            default:
                return;
        }
    }

    @Override
    public void onRefresh() {
        if(mainViewModel.getMLocation().getValue() != null) {
            mainViewModel.setMLocation(mainViewModel.getMLocation().getValue());
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Location location = data.getExtras().getParcelable(Constants.LOCATION_KEY);
            mainViewModel.setMLocation(location);
        }
    }

    @Override
    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
        assert destination.getLabel() != null;
        if(destination.getLabel().toString().equals(getResources().getString(R.string.title_today))){
            navView.setBackgroundResource(R.color.colorPrimary);
        }else{
            navView.setBackgroundResource(R.drawable.retangle_top_round);
        }
    }


}
