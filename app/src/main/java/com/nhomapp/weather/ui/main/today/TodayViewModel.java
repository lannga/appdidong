package com.nhomapp.weather.ui.main.today;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nhomapp.weather.models.reminder.Reminder;

public class TodayViewModel extends ViewModel {

    private MutableLiveData<Reminder> reminder;

    public TodayViewModel() {
        super();
        reminder = new MutableLiveData<>();
        reminder.setValue(new Reminder());
    }

    public void setReminder(Reminder reminder){

    }

}