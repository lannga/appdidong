package com.nhomapp.weather.ui.search;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.nhomapp.weather.models.City;
import com.nhomapp.weather.persistence.AppDatabase;
import com.nhomapp.weather.persistence.CityAccess;

import java.util.List;

public class SearchViewModel extends AndroidViewModel {
    private MutableLiveData<List<City>> searchList;
    private String searchText = "";
    private boolean loading = false;
    private CityAccess cityAccess;

    public SearchViewModel(@NonNull Application application) {
        super(application);
        searchList = new MutableLiveData<>();
        cityAccess = AppDatabase.getInstance(application).cityAccess();
    }

    public LiveData<List<City>> getSearchList(){
        return searchList;
    }

    public void filterCity(String text){
        searchText = text;
        if(loading) {
            return;
        }
        if(searchText.isEmpty()){
            return;
        }
        new FilterCityTask().execute();
    }

    class FilterCityTask extends AsyncTask<Void, Void, List<City>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = true;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected List<City> doInBackground(Void... voids) {
            List<City> list = cityAccess.searchCity(searchText);
            return list;
        }

        @Override
        protected void onPostExecute(List<City> filterList) {
            super.onPostExecute(filterList);
            searchList.setValue(filterList);
            Log.d("abcdfls", filterList.size()+"");
            loading = false;
        }
    }
}
