package com.nhomapp.weather.ui.main.hour;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.LineChart;
import com.nhomapp.weather.R;
import com.nhomapp.weather.adapters.DailyAdapter;
import com.nhomapp.weather.adapters.HourAdapter;
import com.nhomapp.weather.models.DataResponse;
import com.nhomapp.weather.models.MoreInformation;
import com.nhomapp.weather.ui.main.MainActivity;
import com.nhomapp.weather.ui.main.MainViewModel;
import com.nhomapp.weather.util.ChartUtil;

import java.util.ArrayList;
import java.util.List;

public class HourFragment extends Fragment {

    private MainViewModel mainViewModel;
    private List<MoreInformation> hourList;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_hour, container, false);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainViewModel =  mainActivity.getMainViewModel();
        LineChart chart = root.findViewById(R.id.chart);
        hourList = new ArrayList<>();
        HourAdapter hourAdapter = new HourAdapter(hourList);
        RecyclerView hour = root.findViewById(R.id.hour_list);
        hour.setLayoutManager(new LinearLayoutManager(container.getContext()));
        hour.setAdapter(hourAdapter);
        mainViewModel.getDataResponse().observe(getViewLifecycleOwner(), new Observer<DataResponse>() {
            @Override
            public void onChanged(DataResponse dataResponse) {
                ChartUtil.createHour(dataResponse.hourly, chart);
                hourList.clear();
                hourList.addAll(dataResponse.hourly);
                hourAdapter.notifyDataSetChanged();
            }
        });
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
