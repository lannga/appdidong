package com.nhomapp.weather.ui.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.nhomapp.weather.R;
import com.nhomapp.weather.adapters.CitySearchListAdapter;
import com.nhomapp.weather.models.City;
import com.nhomapp.weather.ui.BaseActivity;


import java.util.ArrayList;


public class SearchActivity extends BaseActivity implements TextWatcher{

    private SearchViewModel searchViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_search);
        super.onCreate(savedInstanceState);
        EditText editText = findViewById(R.id.search_edt);
        editText.addTextChangedListener(this);
        editText.requestFocus();
        RecyclerView listView = findViewById(R.id.cities_lv);
        listView.setLayoutManager(new LinearLayoutManager(this));
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        final CitySearchListAdapter adapter = new CitySearchListAdapter(new ArrayList<>(), this);
        listView.setAdapter(adapter);
        searchViewModel.getSearchList().observe(this, filterList -> {
            Log.d("anc",filterList.size()+"");
            adapter.setList(filterList);
        });
        searchViewModel.filterCity("");
    }


    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        searchViewModel.filterCity(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onItemClick(City city) {
        Location location = new Location(city.getAddress());
        location.setLatitude(city.lat);
        location.setLongitude(city.lon);
        Intent intent = new Intent();
        intent.putExtra("location", location);
        setResult(2, intent);
        finish();
    }
}

