package com.nhomapp.weather.requests;

import com.nhomapp.weather.models.DataResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface WeatherApi {
    @GET("onecall")
    Call<DataResponse> getAll(@QueryMap Map<String, Object> params);
}